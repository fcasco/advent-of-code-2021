import argparse


class EnergyField:
    def __init__(self, energies=None):
        self.energies = energies or [[]]
        self.flashes = 0
        self.steps = 0

    def do_next_step(self):
        """
            >>> energy_field = EnergyField([
            ...     [1, 2],
            ...     [3, 9],
            ...     ])
            >>> energy_field.do_next_step()
            >>> energy_field.energies
            [[3, 4], [5, 0]]
            >>> energy_field.flashes
            1

            >>> energy_field = EnergyField([
            ...     [1, 1, 1, 1, 1],
            ...     [1, 9, 9, 9, 1],
            ...     [1, 9, 1, 9, 1],
            ...     [1, 9, 9, 9, 1],
            ...     [1, 1, 1, 1, 1],
            ...     ])
            >>> energy_field.do_next_step()
            >>> ' '.join(''.join(str(x) for x in y) for y in energy_field.energies)
            '34543 40004 50005 40004 34543'
            >>> energy_field.flashes
            9

            >>> energy_field = EnergyField([[9, 9], [9, 9]])
            >>> energy_field.do_next_step()
            >>> energy_field.energies
            [[0, 0], [0, 0]]
            >>> energy_field.flashes
            4
        """
        new_energies = [[x + 1 for x in y] for y in self.energies]
        flashes = 0
        h = len(new_energies)
        w = len(new_energies[0])
        deltas = [(i, j) for i in (-1, 0, 1) for j in (-1, 0, 1)
                  if (i, j) != (0, 0)]

        flashes = set()
        new_flashes = True           # just to enter the loop
        while new_flashes:
            new_flashes = False
            for y in range(h):
                for x in range(w):
                    if (x, y) in flashes:
                        continue

                    if new_energies[y][x] > 9:
                        flashes.add((x, y))
                        new_flashes = True

                        for dx, dy in ((x + i, y + j) for i, j in deltas):
                            if 0 <= dx < w and 0 <= dy < h:
                                new_energies[dy][dx] += 1

        for x, y in flashes:
            new_energies[y][x] = 0

        self.flashes += len(flashes)
        self.energies = new_energies

    def advance(self, steps=0):
        for step in range(steps):
            self.do_next_step()

    def get_big_flash_step(self):
        """
            >>> energy_field = EnergyField([[9, 9], [9, 9]])
            >>> energy_field.get_big_flash_step()
            1
        """
        big_flash = all(all(e == 0 for e in line) for line in self.energies)
        big_flash_step = 0
        while not big_flash:
            big_flash_step += 1
            self.do_next_step()
            big_flash = all(all(e == 0 for e in line) for line in self.energies)

        return big_flash_step


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        energies = tuple(tuple(int(x) for x in line.strip()) for line in input_data.readlines())

    energy_field = EnergyField(energies)
    energy_field.advance(steps=100)
    answer_one = energy_field.flashes
    print(f'Day 11: Dumbo Octopus: Part 1: {answer_one}')

    energy_field = EnergyField(energies)
    answer_two = energy_field.get_big_flash_step()
    print(f'Day 11: Dumbo Octopus: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 11: Dumbo Octopus')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
