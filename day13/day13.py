import argparse
import logging


logging.basicConfig(filename='/tmp/aoc12021_13.log', format='%(levelname)s:%(message)s',
                    encoding='utf-8', level=logging.DEBUG)


def dots_to_str(dots):
    w = max(x[0] for x in dots)
    h = max(x[1] for x in dots)
    return '\n'.join(''.join('▇' if (x, y) in dots else '.' for x in range(w + 1))
                     for y in range(h + 1))


def fold_points_one(dots, instructions):
    """
          0 1 2 3
        0 - - - -
        1 - - - -
        2 - - - -
        3 - - - -
        4 - - - -
        5 - - - -
        6 - - - -
        7 - - - -
        8 - - - -

        >>> fold_points_one({(0, 1), (0, 2), (0, 3)}, [('y', 4)])
        {(0, 1), (0, 2), (0, 3)}

        >>> fold_points_one({(0, 1), (0, 2), (0, 4)}, [('y', 3)])
        {(0, 1), (0, 2)}

        >>> fold_points_one({(0, 1), (0, 5), (0, 7)}, [('y', 4)])
        {(0, 1), (0, 3)}

        >>> fold_points_one({(0, 1), (3, 5), (2, 7)}, [('y', 4), ('x', 2)])
        {(0, 1), (1, 3)}
    """
    for axis, value in instructions:
        logging.debug(f'\n{dots_to_str(dots)}')

        logging.debug(f'{axis} {value}')
        dots = set((2 * value - x[0] if axis == 'x' and x[0] > value else x[0],
                    2 * value - x[1] if axis == 'y' and x[1] > value else x[1])
                   for x in dots
                   if x[0 if axis == 'x' else 1] != value)

    logging.debug(f'\n{dots_to_str(dots)}')
    return dots


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        dots = set()
        instructions = []
        for line in input_data:
            dots_line = tuple(int(x) for x in line.strip().split(',') if x)
            if not dots_line:
                break
            dots.add(dots_line)

        for line in input_data:
            axis, value = line.strip().split()[-1].split('=')
            instructions.append((axis, int(value)))

    answer_one = len(fold_points_one(dots, instructions[:1]))
    print(f'Day 13: Transparent Origami: Part 1: {answer_one}')

    answer_two = dots_to_str(fold_points_one(dots, instructions))
    print(f'Day 13: Transparent Origami: Part 2: \n{answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 13: Transparent Origami')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
