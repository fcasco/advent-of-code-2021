import argparse


def sonar_sweep_one(measurements=[]):
    """
        >>> sonar_sweep_one()
        0

        >>> sonar_sweep_one([1])
        0

        >>> sonar_sweep_one([1, 2])
        1

        >>> sonar_sweep_one([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
        7
    """
    return len([x for x in range(1, len(measurements))
                if measurements[x - 1] < measurements[x]])


def sonar_sweep_two(measurements=[]):
    """
        >>> sonar_sweep_two()
        0

        >>> sonar_sweep_two([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
        5
    """
    return sonar_sweep_one([sum(measurements[x:x + 3])
                           for x in range(0, len(measurements) - 2)])


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        measurements = [int(x) for x in input_data.readlines()]

    answer_one = sonar_sweep_one(measurements)
    print(f'Day 01: Sonar Sweep: Part 1: {answer_one}')

    answer_two = sonar_sweep_two(measurements)
    print(f'Day 01: Sonar Sweep: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 1: Sonar Sweep')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
