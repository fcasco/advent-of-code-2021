import 'package:test/test.dart';

import 'day01.dart';


void main() {
    test('day01_part_one', () {
      expect(sonar_sweep_one([199, 200, 208, 210, 200, 207, 240, 269, 260, 263]),
             equals(7));
    }, tags: ['day01', 'part_one']);

    test('day01_part_two', () {
      expect(sonar_sweep_two([199, 200, 208, 210, 200, 207, 240, 269, 260, 263]),
             equals(5));
    }, tags: ['day01', 'part_two']);
}
