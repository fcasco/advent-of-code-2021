import 'dart:io';


/// Day 01 part 1 solution
int sonar_sweep_one(List<int> measurements) {
  var increases = 0;
  var prev_measurement = measurements[0];
  measurements.forEach((measurement) {
    if (prev_measurement < measurement) {
      increases++;
    }
    prev_measurement = measurement;
  });
  return increases;
}


/// Day 01 part 2 solution
int sonar_sweep_two(List<int> measurements) {
  var grouped_measurements = <int>[];
  for (var i = 0; i < measurements.length - 2; i++) {
    grouped_measurements.add(measurements.sublist(i, i + 3).reduce((v, m) => v + m));
  }

  return sonar_sweep_one(grouped_measurements);
}


void main(List<String> args) async {
  final input_filename = args[0];
  final data = await File(input_filename).readAsLines();
  final measurements = data.map((x) => int.parse(x)).toList();

  final answer_one = sonar_sweep_one(measurements);
  print('Day 01: Sonar Sweep: Part 1: ${answer_one}');

  final answer_two = sonar_sweep_two(measurements);
  print('Day 01: Sonar Sweep: Part 2: ${answer_two}');
}
