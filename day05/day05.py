import argparse


def hydrothermal_venture(lines=[], mode='part_one'):
    """
        >>> hydrothermal_venture()
        0

        >>> hydrothermal_venture((
        ... '0,9 -> 5,9',
        ... ))
        0

        >>> hydrothermal_venture((
        ... '0,9 -> 5,9',
        ... '8,0 -> 0,8',
        ... '9,4 -> 3,4',
        ... '2,2 -> 2,1',
        ... '7,0 -> 7,4',
        ... '6,4 -> 2,0',
        ... '0,9 -> 2,9',
        ... '3,4 -> 1,4',
        ... '0,0 -> 8,8',
        ... '5,5 -> 8,2',
        ... ))
        5

        >>> hydrothermal_venture((
        ... '0,9 -> 5,9',
        ... '8,0 -> 0,8',
        ... '9,4 -> 3,4',
        ... '2,2 -> 2,1',
        ... '7,0 -> 7,4',
        ... '6,4 -> 2,0',
        ... '0,9 -> 2,9',
        ... '3,4 -> 1,4',
        ... '0,0 -> 8,8',
        ... '5,5 -> 8,2',
        ... ), mode='part_two')
        12
    """
    vents_points = {}
    for line in lines:
        point_a, point_b = (tuple(int(x) for x in x.strip().split(','))
                            for x in line.split('->'))

        if mode == 'part_one':
            if point_a[0] != point_b[0] and point_a[1] != point_b[1]:
                continue

        delta = tuple((point_b[x] - point_a[x]) / abs(point_b[x] - point_a[x])
                      if point_a[x] != point_b[x] else 0
                      for x in (0, 1))

        vent_point = point_a
        while vent_point != point_b:
            vents_points[vent_point] = vents_points.get(vent_point, 0) + 1
            vent_point = tuple(vent_point[x] + delta[x] for x in (0, 1))

        vents_points[point_b] = vents_points.get(point_b, 0) + 1

    return len([k for k, v in vents_points.items() if v > 1])


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        lines = [x.strip() for x in input_data.readlines()]

    answer_one = hydrothermal_venture(lines)
    print(f'Day 05: Hydrothermal Venture: Part 1: {answer_one}')

    answer_two = hydrothermal_venture(lines, mode='part_two')
    print(f'Day 05: Hydrothermal Venture: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 5: Hydrothermal Venture')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
