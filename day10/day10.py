import argparse


class Linter:
    char_pairs = ('([{<', ')]}>')
    error_points = {')': 3, ']': 57, '}': 1197, '>': 25137}
    completion_points = {')': 1, ']': 2, '}': 3, '>': 4}

    def __init__(self, lines=None):
        if not lines:
            lines = tuple()

        self.lines = lines

    def check_line(self, line):
        """
            >>> Linter().check_line('([<>])')

            >>> Linter().check_line('(]')
            ']'

            >>> Linter().check_line('{([(<{}[<>[]}>{[]{[(<()>')
            '}'
        """
        expected_chars = []
        for char in line:
            if char in self.char_pairs[0]:
                i = self.char_pairs[0].index(char)
                expected_chars.append(self.char_pairs[1][i])
            elif char in self.char_pairs[1]:
                if char != expected_chars[-1]:
                    return char
                else:
                    expected_chars.pop()

        return None

    def get_errors(self):
        """
            >>> Linter((
            ... '([])',
            ... '{()()()}',
            ... '<([{}])>',
            ... )).get_errors()
            {}

            >>> Linter((
            ... '([)',
            ... '({)',
            ... '(<)',
            ... )).get_errors()
            {')': 3}

            >>> Linter((
            ... '([>',
            ... '{()]',
            ... '<()[]>',
            ... '<()]>[',
            ... )).get_errors()
            {'>': 1, ']': 2}
        """
        errors = {}
        for line in self.lines:
            error_char = self.check_line(line)
            if error_char:
                errors[error_char] = errors.get(error_char, 0) + 1

        return errors

    def get_completions(self):
        """
            >>> Linter((
            ... '[({(<(())[]>[[{[]{<()<>>',
            ... '[(()[<>])]({[<{<<[]>>(',
            ... '{([(<{}[<>[]}>{[]{[(<()>',
            ... '(((({<>}<{<{<>}{[]{[]{}',
            ... '[[<[([]))<([[{}[[()]]]',
            ... '[{[{({}]{}}([{[{{{}}([]',
            ... '{<[[]]>}<{[{[{[]{()[[[]',
            ... '[<(<(<(<{}))><([]([]()',
            ... '<{([([[(<>()){}]>(<<{{',
            ... '<{([{{}}[<[[[<>{}]]]>[]]',
            ... )).get_completions()
            ['}}]])})]', ')}>]})', '}}>}>))))', ']]}}]}]}>', '])}>']
        """
        completions = []
        for line in self.lines:
            error = False
            expected_chars = []
            for char in line:
                if char in self.char_pairs[0]:
                    i = self.char_pairs[0].index(char)
                    expected_chars.append(self.char_pairs[1][i])
                elif char in self.char_pairs[1]:
                    if char != expected_chars[-1]:
                        error = True
                    else:
                        expected_chars.pop()

                if error:
                    continue

            if not error:
                completions.append(''.join(expected_chars[::-1]))

        return completions

    def get_syntax_error_score(self):
        """
            >>> Linter((
            ... '[({(<(())[]>[[{[]{<()<>>',
            ... '[(()[<>])]({[<{<<[]>>(',
            ... '{([(<{}[<>[]}>{[]{[(<()>',
            ... '(((({<>}<{<{<>}{[]{[]{}',
            ... '[[<[([]))<([[{}[[()]]]',
            ... '[{[{({}]{}}([{[{{{}}([]',
            ... '{<[[]]>}<{[{[{[]{()[[[]',
            ... '[<(<(<(<{}))><([]([]()',
            ... '<{([([[(<>()){}]>(<<{{',
            ... '<{([{{}}[<[[[<>{}]]]>[]]',
            ... )).get_syntax_error_score()
            26397
        """
        errors = self.get_errors()
        return sum(self.error_points.get(k, 0) * v for k, v in errors.items())

    def score_completion(self, completion):
        """
            >>> Linter().score_completion('}}]])})]')
            288957
        """
        score = 0
        for char in completion:
            score = (score * 5) + self.completion_points[char]
        return score

    def get_completions_score(self):
        """
            >>> Linter((
            ... '[({(<(())[]>[[{[]{<()<>>',
            ... '[(()[<>])]({[<{<<[]>>(',
            ... '{([(<{}[<>[]}>{[]{[(<()>',
            ... '(((({<>}<{<{<>}{[]{[]{}',
            ... '[[<[([]))<([[{}[[()]]]',
            ... '[{[{({}]{}}([{[{{{}}([]',
            ... '{<[[]]>}<{[{[{[]{()[[[]',
            ... '[<(<(<(<{}))><([]([]()',
            ... '<{([([[(<>()){}]>(<<{{',
            ... '<{([{{}}[<[[[<>{}]]]>[]]',
            ... )).get_completions_score()
            288957
        """
        completions = self.get_completions()
        scores = [self.score_completion(x) for x in completions]
        return sorted(scores)[len(scores) // 2]


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        lines = tuple(x.strip() for x in input_data.readlines())

    linter = Linter(lines)
    answer_one = linter.get_syntax_error_score()
    print(f'Day 10: Syntax Scoring: Part 1: {answer_one}')

    answer_two = linter.get_completions_score()
    print(f'Day 10: Syntax Scoring: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 10: Syntax Scoring')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
