import argparse
import functools


def get_lowest_points(heightmap=None):
    """
        >>> get_lowest_points()
        []

        >>> get_lowest_points((
        ... (1, 1, 1),
        ... (1, 0, 1),
        ... (1, 1, 1),
        ... ))
        [(1, 1)]

        >>> get_lowest_points((
        ... (2, 1, 9, 9, 9, 4, 3, 2, 1, 0),
        ... (3, 9, 8, 7, 8, 9, 4, 9, 2, 1),
        ... (9, 8, 5, 6, 7, 8, 9, 8, 9, 2),
        ... (8, 7, 6, 7, 8, 9, 6, 7, 8, 9),
        ... (9, 8, 9, 9, 9, 6, 5, 6, 7, 8),
        ... ))
        [(1, 0), (9, 0), (2, 2), (6, 4)]
    """
    if not heightmap:
        return []

    h = len(heightmap)
    w = len(heightmap[0])
    lowest_points = []
    for y in range(h):
        for x in range(w):
            adjacents_coords = [(x + i, y) for i in (-1, 1)] + [(x, y + j) for j in (-1, 1)]
            adjacents = [heightmap[y][x] for x, y in adjacents_coords
                         if 0 <= x < w and 0 <= y < h]
            current_height = heightmap[y][x]
            if min(adjacents) > current_height:
                lowest_points.append((x, y))

    return lowest_points


def get_risk_level(heightmap, lowest_points):
    """
        >>> get_risk_level((
        ... (2, 1, 9, 9, 9, 4, 3, 2, 1, 0),
        ... (3, 9, 8, 7, 8, 9, 4, 9, 2, 1),
        ... (9, 8, 5, 6, 7, 8, 9, 8, 9, 2),
        ... (8, 7, 6, 7, 8, 9, 6, 7, 8, 9),
        ... (9, 8, 9, 9, 9, 6, 5, 6, 7, 8),
        ... ), [])
        0

        >>> get_risk_level((
        ... (2, 1, 9, 9, 9, 4, 3, 2, 1, 0),
        ... (3, 9, 8, 7, 8, 9, 4, 9, 2, 1),
        ... (9, 8, 5, 6, 7, 8, 9, 8, 9, 2),
        ... (8, 7, 6, 7, 8, 9, 6, 7, 8, 9),
        ... (9, 8, 9, 9, 9, 6, 5, 6, 7, 8),
        ... ), [(1, 0), (9, 0), (2, 2), (6, 4)])
        15
    """
    return sum(heightmap[y][x] + 1 for x, y in lowest_points)


def get_basins(heightmap, lowest_points):
    """
        >>> get_basins((), [])
        []

        >>> get_basins((
        ... (9, 9, 9),
        ... (9, 0, 9),
        ... (9, 9, 9),
        ... ), [(1, 1)])
        [{(1, 1)}]

        >>> basins = get_basins((
        ... (2, 1, 9, 9, 9, 4, 3, 2, 1, 0),
        ... (3, 9, 8, 7, 8, 9, 4, 9, 2, 1),
        ... (9, 8, 5, 6, 7, 8, 9, 8, 9, 2),
        ... (8, 7, 6, 7, 8, 9, 6, 7, 8, 9),
        ... (9, 8, 9, 9, 9, 6, 5, 6, 7, 8),
        ... ), [(1, 0), (9, 0), (2, 2), (6, 4)])
        >>> basins[0]
        {(1, 0), (0, 1), (0, 0)}

        >>> basins[1]
        {(9, 0), (8, 1), (6, 1), (7, 0), (9, 2), (8, 0), (5, 0), (6, 0), (9, 1)}

        >>> len(basins[2])
        14

        >>> basins[3]
        {(7, 4), (8, 4), (5, 4), (6, 4), (7, 3), (8, 3), (7, 2), (6, 3), (9, 4)}

        >>> len(basins)
        4
    """
    if not heightmap:
        return []

    # from pudb import set_trace; set_trace()
    adjacents_delta = tuple((x, 0) for x in (-1, 1)) + tuple((0, y) for y in (-1, 1))
    h = len(heightmap)
    w = len(heightmap[0])
    basins = []
    for lowest_point in lowest_points:
        new_basin = set()

        points_to_check = {lowest_point}
        while points_to_check:
            point = points_to_check.pop()
            new_basin.add(point)

            for delta in adjacents_delta:
                dx = point[0] + delta[0]
                dy = point[1] + delta[1]
                if 0 <= dx < w and 0 <= dy < h and (delta != (0, 0)) \
                        and (dx, dy) not in new_basin \
                        and heightmap[dy][dx] < 9:
                    points_to_check.add((dx, dy))

        basins.append(new_basin)

    return basins


def get_smoke_basin_two(basins):
    """
        >>> get_smoke_basin_two(({(1, 1)}, {(2, 2), (1, 1)}, {(3, 3), (3, 2), (3, 1)}))
        6

        >>> get_smoke_basin_two((
        ...     {(1, 0), (0, 1), (0, 0)},
        ...     {(9, 0), (8, 1), (6, 1), (7, 0), (9, 2), (8, 0), (5, 0), (6, 0), (9, 1)},
        ...     {(1, 2), (2, 1), (4, 3), (3, 1), (4, 1), (0, 3), (4, 2), (1, 4), (2, 3),
        ...         (3, 3), (2, 2), (3, 2), (1, 3), (5, 2)},
        ...     {(7, 4), (8, 4), (5, 4), (6, 4), (7, 3), (8, 3), (7, 2), (6, 3), (9, 4)},
        ... ))
        1134
    """
    return functools.reduce(lambda x, y: x * y,
                            sorted([len(x) for x in basins], reverse=True)[:3])


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        heightmap = tuple(tuple(int(x) for x in line.strip())
                          for line in input_data.readlines())

    lowest_points = get_lowest_points(heightmap)
    answer_one = get_risk_level(heightmap, lowest_points)
    print(f'Day 09: Smoke Basin: Part 1: {answer_one}')

    answer_two = get_smoke_basin_two(get_basins(heightmap, lowest_points))
    print(f'Day 09: Smoke Basin: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 9: Smoke Basin')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
