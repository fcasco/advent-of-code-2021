import argparse


def the_treachery_of_whales_one(positions=None):
    """
        >>> the_treachery_of_whales_one()
        0

        >>> the_treachery_of_whales_one((16, 1, 2, 0, 4, 2, 7, 1, 2, 14))
        37
    """
    if not positions:
        return 0

    median = sorted(positions)[len(positions) // 2]
    fuel_cost = sum(abs(x - median) for x in positions)
    return fuel_cost


def the_treachery_of_whales_two(positions=None):
    """
        >>> the_treachery_of_whales_two()
        0

        >>> the_treachery_of_whales_two((16, 1, 2, 0, 4, 2, 7, 1, 2, 14))
        168
    """
    if not positions:
        return 0

    max_position = max(positions)
    fuel_cost = None

    def calculate_cost(position, target):
        distance = abs(target - position)
        cost = distance * (distance + 1) / 2
        return cost

    for align_to in range(min(positions), max_position + 1):
        new_fuel_cost = sum(calculate_cost(x, align_to) for x in positions)
        if fuel_cost is None or new_fuel_cost < fuel_cost:
            fuel_cost = new_fuel_cost

    return int(fuel_cost)


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        positions = tuple(int(x) for x in input_data.readline().split(','))

    answer_one = the_treachery_of_whales_one(positions)
    print(f'Day 07: The Treachery of Whales: Part 1: {answer_one}')

    answer_two = the_treachery_of_whales_two(positions)
    print(f'Day 07: The Treachery of Whales: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 7: The Treachery of Whales')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
