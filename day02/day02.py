import argparse


def dive_one(instructions=[]):
    """
        >>> dive_one()
        0

        >>> dive_one(('forward 5', 'down 5'))
        25

        >>> dive_one(('forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2'))
        150
    """
    position = 0
    depth = 0
    for instruction in instructions:
        command, argument = instruction.split()
        if command == 'forward':
            position += int(argument)
        elif command == 'down':
            depth += int(argument)
        elif command == 'up':
            depth -= int(argument)

    return position * depth


def dive_two(instructions=[]):
    """
        >>> dive_two()
        0

        >>> dive_two(('forward 5', 'down 5', 'forward 1'))
        30

        >>> dive_two(('forward 5', 'down 5', 'forward 8', 'up 3', 'down 8', 'forward 2'))
        900
    """
    aim = 0
    depth = 0
    position = 0
    for instruction in instructions:
        command, argument = instruction.split()
        argument = int(argument)
        if command == 'forward':
            position += argument
            depth += aim * argument
        elif command == 'down':
            aim += argument
        elif command == 'up':
            aim -= argument

    return position * depth


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        instructions = [x.strip() for x in input_data.readlines()]

    answer_one = dive_one(instructions)
    print(f'Day 02: Dive!: Part 1: {answer_one}')

    answer_two = dive_two(instructions)
    print(f'Day 02: Dive!: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 2: Dive!')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
