import argparse


def seven_segment_search_one(signals=None):
    """
        >>> seven_segment_search_one()
        0

        >>> seven_segment_search_one((
        ...     (
        ...     ('acedgfb', 'cdfbe', 'gcdfa', 'fbcad', 'dab', 'cefabd', 'cdfgeb',
        ...      'eafb', 'cagedb', 'ab'),
        ...     ('cdfeb', 'fcadb', 'cdfeb', 'cdbaf')
        ...     ),
        ... ))
        0

        >>> seven_segment_search_one((
        ...     (
        ...     ('acedgfb', 'cdfbe', 'gcdfa', 'fbcad', 'dab', 'cefabd', 'cdfgeb',
        ...      'eafb', 'cagedb', 'ab'),
        ...     ('cd', 'fcab', 'cdf', 'abcdefg')
        ...     ),
        ... ))
        4
    """
    if not signals:
        return 0

    return sum(len([x for x in signal[1] if len(x) in (2, 4, 3, 7)]) for signal in signals)


def decode_patterns(patterns=None):
    """
        >>> segments_map = decode_patterns(('abcefg', 'cf', 'acdeg', 'acdfg', 'bcdf',
        ...                                 'abdfg', 'abdefg', 'acf', 'abcdefg', 'abcdfg'))
        >>> segments_map['abcefg']
        0
        >>> segments_map['cf']
        1
        >>> segments_map['acdeg']
        2
        >>> segments_map['acdfg']
        3
        >>> segments_map['bcdf']
        4
        >>> segments_map['abdfg']
        5
        >>> segments_map['abdefg']
        6
        >>> segments_map['acf']
        7
        >>> segments_map['abcdefg']
        8
        >>> segments_map['abcdfg']
        9
    """
    if not patterns:
        return

    patterns = [set(x) for x in patterns]

    numbers_to_segments = {
            # 1 is the only with 2 segments
            1: [x for x in patterns if len(x) == 2][0],

            # 4 is the only with 4 segments
            4: [x for x in patterns if len(x) == 4][0],

            # 7 is the only with 3 segments
            7: [x for x in patterns if len(x) == 3][0],

            # 8 is the only with 7 segments
            8: [x for x in patterns if len(x) == 7][0],
    }

    # normal segments to segments in this patterns
    segments_map = {
            # d is the only segment shared between 2, 3, 5 and 4 which have 4 or 5 segments
            'd': list(set.intersection(*[x for x in patterns if len(x) in (4, 5)]))[0],
    }

    numbers_to_segments[0] = [x for x in patterns
                              if len(x) == 6
                              and segments_map['d'] not in x][0]

    numbers_to_segments[9] = [x for x in patterns
                              if len(x) == 6
                              and segments_map['d'] in x
                              and numbers_to_segments[1].issubset(x)][0]

    numbers_to_segments[6] = [x for x in patterns
                              if len(x) == 6
                              and x != numbers_to_segments[0]
                              and x != numbers_to_segments[9]][0]

    numbers_to_segments[3] = [x for x in patterns
                              if len(x) == 5
                              and numbers_to_segments[1].issubset(x)][0]

    numbers_to_segments[5] = [x for x in patterns
                              if len(x) == 5
                              and x.issubset(numbers_to_segments[6])][0]

    numbers_to_segments[2] = [x for x in patterns
                              if len(x) == 5
                              and x != numbers_to_segments[3]
                              and x != numbers_to_segments[5]][0]

    return {''.join(sorted(v)): k for k, v in sorted(numbers_to_segments.items())}


def seven_segment_search_two(signals=None):
    """
        >>> seven_segment_search_two()
        0

        >>> seven_segment_search_two((
        ...     (
        ...     ('acedgfb', 'cdfbe', 'gcdfa', 'fbcad', 'dab', 'cefabd', 'cdfgeb',
        ...      'eafb', 'cagedb', 'ab'),
        ...     ('cdfeb', 'fcadb', 'cdfeb', 'cdbaf')
        ...     ),
        ... ))
        5353
    """
    if not signals:
        return 0

    values_sum = 0
    for signal in signals:
        segments_to_numbers = decode_patterns(signal[0])
        values_sum += int(''.join(str(segments_to_numbers[''.join(sorted(x))])
                                  for x in signal[1]))

    return values_sum


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        signals = tuple(tuple(tuple(x.strip() for x in x.split()) for x in line.split('|'))
                        for line in input_data.readlines())

    answer_one = seven_segment_search_one(signals)
    print(f'Day 08: Seven Segment Search: Part 1: {answer_one}')

    answer_two = seven_segment_search_two(signals)
    print(f'Day 08: Seven Segment Search: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 8: Seven Segment Search')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
