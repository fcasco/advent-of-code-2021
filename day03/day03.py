import argparse
import collections


def binary_diagnostic_one(report=[]):
    """
        >>> binary_diagnostic_one(('00100', '11110', '10110', '10111', '10101', '01111',
        ...                        '00111', '11100', '10000', '11001', '00010', '01010',
        ... ))
        198
    """
    gamma_rate = []
    epsilon_rate = []
    for i in range(len(report[0])):
        bit_counter = collections.Counter(x[i] for x in report)
        gamma_rate.append(bit_counter.most_common()[0][0])
        epsilon_rate.append(bit_counter.most_common()[-1][0])

    gamma_rate = int(''.join(gamma_rate), 2)
    epsilon_rate = int(''.join(epsilon_rate), 2)
    return gamma_rate * epsilon_rate


def binary_diagnostic_two(report=[]):
    """
        >>> binary_diagnostic_two(('00000', '11111'))
        0

        >>> binary_diagnostic_two(('00100', '11110', '10110', '10111', '10101', '01111',
        ...                        '00111', '11100', '10000', '11001', '00010', '01010',
        ... ))
        230
    """
    numbers_len = len(report[0])

    o_rating_values = report
    for i in range(numbers_len):
        bit_counter = collections.Counter(x[i] for x in o_rating_values)
        most_common_bits = bit_counter.most_common()
        most_common_bit = most_common_bits[0][0]

        # the default ordering of most_common is not what the spec requires
        if len(most_common_bits) > 1 and most_common_bits[0][1] == most_common_bits[1][1]:
            most_common_bit = '1'

        o_rating_values = [x for x in o_rating_values if x[i] == most_common_bit]

        if len(o_rating_values) == 1:
            break

    co2_rating_values = report
    for i in range(numbers_len):
        bit_counter = collections.Counter(x[i] for x in co2_rating_values)
        most_common_bits = bit_counter.most_common()
        least_common_bit = most_common_bits[-1][0]

        # the default ordering of most_common is not what the spec requires
        if len(most_common_bits) > 1 and most_common_bits[0][1] == most_common_bits[1][1]:
            least_common_bit = '0'

        co2_rating_values = [x for x in co2_rating_values if x[i] == least_common_bit]

        if len(co2_rating_values) == 1:
            break

    o_rating = int(o_rating_values[0], 2)
    co2_rating = int(co2_rating_values[0], 2)
    return o_rating * co2_rating


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        report = [x.strip() for x in input_data.readlines()]

    answer_one = binary_diagnostic_one(report)
    print(f'Day 03: Binary Diagnostic: Part 1: {answer_one}')

    answer_two = binary_diagnostic_two(report)
    print(f'Day 03: Binary Diagnostic: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 3: Binary Diagnostic')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
