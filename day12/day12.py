import argparse
import logging


logging.basicConfig(filename='/tmp/aoc12021_12.log', format='%(levelname)s:%(message)s',
                    encoding='utf-8', level=logging.DEBUG)


def get_paths_one(connections):
    """
        >>> get_paths_one(('start-A', 'A-end'))
        ['start,A,end']

        >>> get_paths_one(('start-A', 'A-b', 'b-end'))
        ['start,A,b,end']

        >>> all_paths = get_paths_one((
        ...     'start-A',
        ...     'start-b',
        ...     'A-c',
        ...     'A-b',
        ...     'b-d',
        ...     'A-end',
        ...     'b-end',
        ... ))
        >>> len(all_paths)
        10

        >>> sorted(all_paths)   # doctest: +ELLIPSIS
        ['start,A,b,A,c,A,end', 'start,A,b,A,end', ... 'start,b,A,end', 'start,b,end']

        >>> len(get_paths_one((
        ...      'dc-end',
        ...      'HN-start',
        ...      'start-kj',
        ...      'dc-start',
        ...      'dc-HN',
        ...      'LN-dc',
        ...      'HN-end',
        ...      'kj-sa',
        ...      'kj-HN',
        ...      'kj-dc',
        ...  )))
        19
    """
    caves_map = {}
    for connection in connections:
        source, destination = connection.split('-')
        if source != 'end':
            caves_map[source] = [destination, ] + caves_map.get(source, [])
        if destination != 'end':
            caves_map[destination] = [source, ] + caves_map.get(destination, [])

    logging.debug(f'Caves map: {caves_map}')

    paths = []
    new_paths = [['start']]
    path_changes = False
    while new_paths or path_changes:
        logging.debug(new_paths)
        paths += new_paths
        new_paths = []
        path_changes = False
        for path in paths:
            last_node = path[-1]
            if last_node == 'end':
                continue

            next_nodes = [x for x in caves_map.get(last_node, [])
                          if x != 'start'
                          and (last_node, x) not in set(zip(path, path[1:]))
                          and not (x.islower() and x in path)]
            logging.debug(f'{path} > {next_nodes}')
            if not next_nodes:
                continue

            path.append(next_nodes.pop())
            path_changes = True

            for next_node in next_nodes:
                new_path = path[:-1] + [next_node]
                new_paths.append(new_path)

    return [','.join(x) for x in paths if x[-1] == 'end']


def node_is_allowed_in_path(node, path):
    """
        >>> node_is_allowed_in_path('start', [])
        True

        >>> node_is_allowed_in_path('start', ['start'])
        False

        >>> node_is_allowed_in_path('end', ['end'])
        False

        >>> node_is_allowed_in_path('A', [])
        True

        >>> node_is_allowed_in_path('b', [])
        True

        >>> node_is_allowed_in_path('b', ['b', 'b'])
        False

        >>> node_is_allowed_in_path('b', ['a', 'b'])
        True

        >>> node_is_allowed_in_path('b', ['a', 'a', 'b'])
        False
    """
    return bool(
            node.isupper()
            or (node in ('start', 'end') and node not in path)
            or (
                node not in ('start', 'end') and
                all(len([y for y in path if y == x]) < 2
                    for x in set(n for n in path if n.islower()))
                or node not in path)
    )


def get_paths_two(connections):
    """
        >>> all_paths = get_paths_two((
        ...     'start-A',
        ...     'start-b',
        ...     'A-c',
        ...     'A-b',
        ...     'b-d',
        ...     'A-end',
        ...     'b-end',
        ... ))
        >>> len(all_paths)
        36

        >>> len(get_paths_two((
        ...      'dc-end',
        ...      'HN-start',
        ...      'start-kj',
        ...      'dc-start',
        ...      'dc-HN',
        ...      'LN-dc',
        ...      'HN-end',
        ...      'kj-sa',
        ...      'kj-HN',
        ...      'kj-dc',
        ...  )))
        103
    """
    logging.debug(f'get_paths_two({connections})')
    caves_map = {}
    for connection in connections:
        source, destination = connection.split('-')
        if source != 'end':
            caves_map[source] = [destination, ] + caves_map.get(source, [])
        if destination != 'end':
            caves_map[destination] = [source, ] + caves_map.get(destination, [])

    logging.debug(f'Caves map: {caves_map}')

    paths = []
    new_paths = [['start']]
    path_changes = False
    while new_paths or path_changes:
        logging.debug(new_paths)
        paths += new_paths
        new_paths = []
        path_changes = False
        for path in paths:
            last_node = path[-1]
            if last_node == 'end':
                continue

            next_nodes = [x for x in caves_map.get(last_node, [])
                          if node_is_allowed_in_path(x, path)]
            logging.debug(f'{path} > {next_nodes}')

            if not next_nodes:
                continue

            path.append(next_nodes.pop())
            path_changes = True

            for next_node in next_nodes:
                new_path = path[:-1] + [next_node]
                new_paths.append(new_path)

    return [','.join(x) for x in paths if x[-1] == 'end']


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        connections = tuple(x.strip() for x in input_data.readlines())

    answer_one = len(get_paths_one(connections))
    print(f'Day 12: Passage Pathing: Part 1: {answer_one}')

    answer_two = len(get_paths_two(connections))
    print(f'Day 12: Passage Pathing: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 12: Passage Pathing')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
