import argparse
import collections
import logging


logging.basicConfig(filename='/tmp/aoc12021_14.log', format='%(levelname)s:%(message)s',
                    encoding='utf-8', level=logging.DEBUG)


def apply_rules_one(template, rules, steps=0):
    """
        >>> apply_rules_one('NNCB', {'CH': 'B', 'HH': 'N'}, 1)
        'NNCB'

        >>> apply_rules_one('NNCB', {'CH': 'B', 'HH': 'N', 'CB': 'H'}, 1)
        'NNCHB'

        >>> apply_rules_one('NNCB', {
        ...     'CH': 'B',
        ...     'HH': 'N',
        ...     'CB': 'H',
        ...     'NH': 'C',
        ...     'HB': 'C',
        ...     'HC': 'B',
        ...     'HN': 'C',
        ...     'NN': 'C',
        ...     'BH': 'H',
        ...     'NC': 'B',
        ...     'NB': 'B',
        ...     'BN': 'B',
        ...     'BB': 'N',
        ...     'BC': 'B',
        ...     'CC': 'N',
        ...     'CN': 'C',
        ...  }, 4)
        'NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB'
    """
    polymer = template

    for i in range(steps):
        logging.debug(polymer)

        pairs = (''.join(x) for x in zip(polymer, polymer[1:]))
        last_element = polymer[-1]
        polymer = ''.join(f'{x[0]}{rules.get(x, "")}' for x in pairs)
        polymer += last_element

    return polymer


def get_elements_count_in_pairs(template, rules, steps=0):
    """
        >>> sorted(
        ...     get_elements_count_in_pairs('NNCB', {'CH': 'B', 'HH': 'N'}, 1)
        ...     .items())
        [('B', 1), ('C', 1), ('N', 2)]

        >>> sorted(
        ...     get_elements_count_in_pairs('NNCB', {'CH': 'B', 'HH': 'N', 'CB': 'H'}, 1)
        ...     .items())
        [('B', 1), ('C', 1), ('H', 1), ('N', 2)]

        >>> sorted(
        ...     get_elements_count_in_pairs('NNCB', {
        ...         'CH': 'B',
        ...         'HH': 'N',
        ...         'CB': 'H',
        ...         'NH': 'C',
        ...         'HB': 'C',
        ...         'HC': 'B',
        ...         'HN': 'C',
        ...         'NN': 'C',
        ...         'BH': 'H',
        ...         'NC': 'B',
        ...         'NB': 'B',
        ...         'BN': 'B',
        ...         'BB': 'N',
        ...         'BC': 'B',
        ...         'CC': 'N',
        ...         'CN': 'C',
        ...      }, 4)
        ...     .items())
        [('B', 23), ('C', 10), ('H', 5), ('N', 11)]

        >>> sorted(
        ...     get_elements_count_in_pairs('NNCB', {
        ...         'CH': 'B',
        ...         'HH': 'N',
        ...         'CB': 'H',
        ...         'NH': 'C',
        ...         'HB': 'C',
        ...         'HC': 'B',
        ...         'HN': 'C',
        ...         'NN': 'C',
        ...         'BH': 'H',
        ...         'NC': 'B',
        ...         'NB': 'B',
        ...         'BN': 'B',
        ...         'BB': 'N',
        ...         'BC': 'B',
        ...         'CC': 'N',
        ...         'CN': 'C',
        ...      }, 10)
        ...     .items())
        [('B', 1749), ('C', 298), ('H', 161), ('N', 865)]

        Counter({'B': 1749, 'N': 865, 'C': 298, 'H': 161})
    """
    rules = {k: (k[0] + v, v + k[1]) for k, v in rules.items()}
    pairs_counter = {}
    for pair in zip(template, template[1:]):
        pairs_counter[''.join(pair)] = pairs_counter.get(''.join(pair), 0) + 1

    for i in range(steps):
        logging.debug(f'{i} {len(pairs_counter)} {pairs_counter}')
        new_pairs_counter = {}
        for old_pair, count in pairs_counter.items():
            for pair in rules.get(old_pair, (old_pair, )):
                new_pairs_counter[pair] = new_pairs_counter.get(pair, 0) + count

        pairs_counter = new_pairs_counter

    elements_counter = {}
    for element in set(''.join(pairs_counter.keys())):
        elements_counter[element] = sum(v for k, v in pairs_counter.items()
                                        if k[0] == element)

    elements_counter[template[-1]] += 1

    return elements_counter


def main(args):
    logging.getLogger().setLevel(logging.DEBUG)
    logging.info('Day 14: Extended Polymerization')
    logging.debug(args)

    input_file = args.input_file
    with open(input_file) as input_data:
        template = next(input_data).strip()

        rules = {k: v for k, v in (x.strip().split(' -> ')
                                   for x in input_data if x.strip())}

    polymer = apply_rules_one(template, rules, steps=10)
    most_common_elements = collections.Counter(polymer).most_common()
    answer_one = most_common_elements[0][1] - most_common_elements[-1][1]
    print(f'Day 14: Extended Polymerization: Part 1: {answer_one}')

    elements_counter = get_elements_count_in_pairs(template, rules, steps=args.steps)
    most_common_elements = sorted(elements_counter.items(), key=lambda x: x[1], reverse=True)
    answer_two = most_common_elements[0][1] - most_common_elements[-1][1]
    print(f'Day 14: Extended Polymerization: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 14: Extended Polymerization')
    args_parser.add_argument('input_file', type=str)
    args_parser.add_argument('steps', type=int)
    args = args_parser.parse_args()
    main(args)
