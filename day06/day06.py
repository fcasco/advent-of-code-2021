import argparse


def lanternfish_one(starting_fishes=(), days=0):
    """
        >>> lanternfish_one()
        0

        >>> lanternfish_one((3, 4, 3, 1, 2), days=1)
        5

        >>> lanternfish_one((3, 4, 3, 1, 2), days=18)
        26

        >>> lanternfish_one((3, 4, 3, 1, 2), days=80)
        5934
    """
    fishes = list(starting_fishes)
    for day in range(days):
        new_fishes = [8 for x in fishes if x == 0]

        fishes = [x - 1 if x > 0 else 6 for x in fishes] + new_fishes

    return len(fishes)


def lanternfish_two(starting_fishes=(), days=0):
    """
        Initial state: 3,4,3,1,2
        After  1 day:  2,3,2,0,1
        After  2 days: 1,2,1,6,0,8

        s>>> lanternfish_two()
        0

        >>> lanternfish_two((3, 4, 3, 1, 2), days=1)
        5

        >>> lanternfish_two((3, 4, 3, 1, 2), days=18)
        26

        >>> lanternfish_two((3, 4, 3, 1, 2), days=80)
        5934

        >>> lanternfish_two((3, 4, 3, 1, 2), days=256)
        26984457539
    """
    fishes = {x: len([y for y in starting_fishes if y == x]) for x in range(9)}

    for day in range(days):
        # old_fishes = fishes
        new_fishes = fishes.get(0, 0)
        fishes = {(k - 1) % 9: v for k, v in fishes.items()}
        fishes[6] += fishes.get(8, 0)
        fishes[8] = new_fishes

    return sum(fishes.values())


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        fishes = tuple(int(x) for x in input_data.readline().split(','))

    answer_one = lanternfish_one(fishes, days=80)
    print(f'Day 06: Lanternfish: Part 1: {answer_one}')

    answer_two = lanternfish_two(fishes, days=256)
    print(f'Day 06: Lanternfish: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 6: Lanternfish')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
