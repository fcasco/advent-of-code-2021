import argparse


class Bingo:
    def __init__(self, numbers=(), boards=()):
        self.numbers = numbers
        self.boards = [x[:] for x in boards]
        self.winner_board = None

    def __str__(self):
        print(f'Numbers: {self.numbers}')
        print(f'Winning board: {self.winner_board}')
        print(f'Winning number: {self.winner_number}')

    def has_board_won(self, board):
        """
            >>> Bingo().has_board_won(())
            False

            >>> Bingo().has_board_won(((1, 2), (3, 4), (5, 6)))
            False

            >>> Bingo().has_board_won(((1, 2), (None, None), (5, 6)))
            True

            >>> Bingo().has_board_won(((1, 2, None, 4), (5, 6, None, 7), (8, 9, None, 0)))
            True
        """
        if not board or len(board) == 0:
            return False

        for line in board:
            if all(x is None for x in line):
                return True

        for i in range(len(board[0])):
            if all(x is None for x in [line[i] for line in board]):
                return True

        return False

    def play(self, until_last_winner=False):
        """
            >>> bingo = Bingo((1, 2, 3), [
            ...     [[1, 2], [3, 4]],
            ...     [[5, 2], [3, 4]],
            ... ])
            >>> bingo.play()
            >>> bingo.winner_board
            [[None, None], [3, 4]]
        """
        for number in self.numbers:
            board_index = 0
            while self.boards and board_index < len(self.boards):
                try:
                    board = self.boards[board_index]
                except IndexError:
                    continue

                for i, line in enumerate(board):
                    board[i] = [None if x == number else x for x in line]

                if self.has_board_won(board):
                    self.winner_board = board
                    self.winner_number = number

                    if until_last_winner:
                        self.boards.pop(board_index)
                    else:
                        return
                else:
                    board_index += 1

            boards_index_to_remove = []
            for board_index, board in enumerate(self.boards):
                for i, line in enumerate(board):
                    board[i] = [None if x == number else x for x in line]

                if self.has_board_won(board):
                    self.winner_board = board
                    self.winner_number = number

                    if not until_last_winner:
                        return

                    boards_index_to_remove.append(board_index)

            for board_index in boards_index_to_remove:
                self.boards.pop(board_index)

            if not self.boards:
                return

    def get_points_for_board(self, board):
        """
            >>> Bingo().get_points_for_board([[None, None], [3, 4]])
            7
        """
        unmarked_numbers = []
        for line in board:
            unmarked_numbers.extend([x if x is not None else 0 for x in line])
        return sum(unmarked_numbers) * getattr(self, 'winner_number', 1)

    def get_winner_board_points(self):
        if not self.winner_board:
            self.play()

        return self.get_points_for_board(self.winner_board)

    def get_last_board_points(self):
        """
            >>> Bingo(
            ...     (1, 2, 3, 4, 5, 6, 7, 8, 9),
            ...     [
            ...     [[1, 2], [3, 4]],
            ...     [[2, 3], [4, 5]],
            ...     [[1, 8], [9, 4]],
            ...     [[5, 6], [7, 1]],
            ...     ]
            ... ).get_last_board_points()
            72
        """
        self.play(until_last_winner=True)
        return self.get_points_for_board(self.winner_board)


def main(args):
    input_file = args.input_file
    with open(input_file) as input_data:
        bingo_numbers = [int(x) for x in input_data.readline().strip().split(',')]
        bingo_boards = []
        new_board = []
        for line in input_data.readlines():
            if line.strip():
                new_board.append([int(x) for x in line.split()])
            else:
                if new_board:
                    bingo_boards.append(new_board)
                new_board = []
        if new_board:
            bingo_boards.append(new_board)

    bingo = Bingo(bingo_numbers, bingo_boards)
    answer_one = bingo.get_winner_board_points()
    print(f'Day 04: Giant Squid: Part 1: {answer_one}')

    bingo = Bingo(bingo_numbers, bingo_boards)
    answer_two = bingo.get_last_board_points()
    print(f'Day 04: Giant Squid: Part 2: {answer_two}')


if __name__ == '__main__':
    args_parser = argparse.ArgumentParser(description='Day 4: Giant Squid')
    args_parser.add_argument('input_file', type=str)
    args = args_parser.parse_args()
    main(args)
