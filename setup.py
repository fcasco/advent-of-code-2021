import setuptools


with open('README.md', 'r', encoding='utf-8') as readme:
    long_description = readme.read()


setuptools.setup(
    name="advent-of-code-2021-fcasco",
    version="0.1.0",
    author="Facundo",
    author_email="fcasco@gmail.com",
    description="Solutions for advent of code 2021",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/fcasco/advent-of-code-2021/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
    python_requires='>=3.9',
)
