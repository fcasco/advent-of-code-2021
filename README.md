# Solutions for Advent of Code 2021

https://adventofcode.com/2021/


## Running the tests for Python

To run the test for the Python solutions use the command `tox`


## Running the tests for Dart

To run the test for the Dart solutions use the command
`dart test day*/*_tests.dart`
